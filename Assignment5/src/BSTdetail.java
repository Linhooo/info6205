import java.util.Map;
import java.util.function.BiFunction;

public interface BSTdetail<Key extends Comparable<Key>, Value> extends BST<Key, Value> {
    Boolean contains(Key var1);

    void putAll(Map<Key, Value> var1);

    void delete(Key var1);

    int size();

    void inOrderTraverse(BiFunction<Key, Value, Void> var1);

    void deleteMin();

    int getMaxDepth();
}
