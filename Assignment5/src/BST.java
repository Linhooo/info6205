import java.util.Set;

public interface BST<Key extends Comparable<Key>, Value> {
    Value get(Key var1);

    Value put(Key var1, Value var2);

    Set<Key> keySet();

    void delete(Key var1);
}