//
// Source code recreated from a .class file by IntelliJ IDEA
// (powered by Fernflower decompiler)
//

import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Random;
import java.util.Set;
import java.util.Map.Entry;
import java.util.function.BiFunction;

public class BSTSimple<Key extends Comparable<Key>, Value> implements BSTdetail<Key, Value> {
    BSTSimple<Key, Value>.Node root;

    public Boolean contains(Key key) {
        return this.get(key) != null ? true : false;
    }

    public void putAll(Map<Key, Value> map) {
        Iterator var3 = map.entrySet().iterator();

        while(var3.hasNext()) {
            Entry<Key, Value> entry = (Entry)var3.next();
            this.put((Key) entry.getKey(), entry.getValue());
        }

    }

    public int size() {
        return this.root != null ? this.root.count : 0;
    }

    public void inOrderTraverse(BiFunction<Key, Value, Void> f) {
        this.doTraverse(0, this.root, f);
    }

    public Value get(Key key) {
        return this.get(this.root, key);
    }

    public Value put(Key key, Value value) {
        BSTSimple<Key, Value>.NodeValue nodeValue = this.put(this.root, key, value);
        if (this.root == null) {
            this.root = nodeValue.node;
        }

        if (nodeValue.value == null) {
            ++this.root.count;
        }

        return nodeValue.value;
    }

    public void deleteMin() {
        this.root = this.deleteMin(this.root);
    }

    public Set<Key> keySet() {
        return null;
    }

    public BSTSimple() {
        this.root = null;
    }

    public BSTSimple(Map<Key, Value> map) {
        this();
        this.putAll(map);
    }

    public void delete(Key key) {
        this.root = this.delete(this.root, key);
    }

    private Value get(BSTSimple<Key, Value>.Node node, Key key) {
        if (node == null) {
            return null;
        } else {
            int cf = key.compareTo(node.key);
            if (cf < 0) {
                return this.get(node.smaller, key);
            } else {
                return cf > 0 ? this.get(node.larger, key) : node.value;
            }
        }
    }

    private BSTSimple<Key, Value>.NodeValue put(BSTSimple<Key, Value>.Node node, Key key, Value value) {
        if (node == null) {
            return new BSTSimple.NodeValue(new BSTSimple.Node(key, value), (Object)null);
        } else {
            int cf = key.compareTo(node.key);
            BSTSimple.NodeValue result;
            if (cf == 0) {
                result = new BSTSimple.NodeValue(node, node.value);
                node.value = value;
                return result;
            } else if (cf < 0) {
                result = this.put(node.smaller, key, value);
                if (node.smaller == null) {
                    node.smaller = result.node;
                }

                if (result.value == null) {
                    ++result.node.count;
                }

                return result;
            } else {
                result = this.put(node.larger, key, value);
                if (node.larger == null) {
                    node.larger = result.node;
                }

                if (result.value == null) {
                    ++result.node.count;
                }

                return result;
            }
        }
    }

    private BSTSimple<Key, Value>.Node delete(BSTSimple<Key, Value>.Node x, Key key) {
        if (x == null) {
            return null;
        } else {
            int cmp = key.compareTo(x.key);
            if (cmp < 0) {
                x.smaller = this.delete(x.smaller, key);
            } else if (cmp > 0) {
                x.larger = this.delete(x.larger, key);
            } else {
                if (x.larger == null) {
                    return x.smaller;
                }

                if (x.smaller == null) {
                    return x.larger;
                }

                BSTSimple<Key, Value>.Node t = x;
                x = this.min(x.larger);
                x.larger = this.deleteMin(t.larger);
                x.smaller = t.smaller;
            }

            x.count = this.size(x.smaller) + this.size(x.larger) + 1;
            return x;
        }
    }

    private BSTSimple<Key, Value>.Node deleteMin(BSTSimple<Key, Value>.Node x) {
        if (x.smaller == null) {
            return x.larger;
        } else {
            x.smaller = this.deleteMin(x.smaller);
            x.count = 1 + this.size(x.smaller) + this.size(x.larger);
            return x;
        }
    }

    private int size(BSTSimple<Key, Value>.Node x) {
        return x == null ? 0 : x.count;
    }

    private BSTSimple<Key, Value>.Node search(Key key) {
        BSTSimple<Key, Value>.Node currentNode = this.root;
        if (currentNode == null) {
            return null;
        } else {
            while(currentNode != null && key != currentNode.key) {
                int cmp = key.compareTo(currentNode.key);
                if (cmp > 0) {
                    currentNode = currentNode.larger;
                } else {
                    currentNode = currentNode.smaller;
                }
            }

            return currentNode;
        }
    }

    private int getMaxDepth(BSTSimple<Key, Value>.Node x) {
        if (x == null) {
            return 0;
        } else {
            int left = this.getMaxDepth(x.smaller);
            int right = this.getMaxDepth(x.larger);
            return 1 + Math.max(left, right);
        }
    }

    public int getMaxDepth() {
        return this.getMaxDepth(this.root);
    }

    private BSTSimple<Key, Value>.Node min(BSTSimple<Key, Value>.Node x) {
        if (x == null) {
            throw new RuntimeException("min not implemented for null");
        } else {
            return x.smaller == null ? x : this.min(x.smaller);
        }
    }

    private void doTraverse(int q, BSTSimple<Key, Value>.Node node, BiFunction<Key, Value, Void> f) {
        if (node != null) {
            if (q < 0) {
                f.apply(node.key, node.value);
            }

            this.doTraverse(q, node.smaller, f);
            if (q == 0) {
                f.apply(node.key, node.value);
            }

            this.doTraverse(q, node.larger, f);
            if (q > 0) {
                f.apply(node.key, node.value);
            }

        }
    }

    private BSTSimple<Key, Value>.Node makeNode(Key key, Value value) {
        return new BSTSimple.Node(key, value);
    }

    private BSTSimple<Key, Value>.Node getRoot() {
        return this.root;
    }

    private void setRoot(BSTSimple<Key, Value>.Node node) {
        if (this.root == null) {
            this.root = node;
            ++this.root.count;
        } else {
            this.root = node;
        }

    }

    private void show(BSTSimple<Key, Value>.Node node, StringBuffer sb, int indent) {
        if (node != null) {
            int i;
            for(i = 0; i < indent; ++i) {
                sb.append("  ");
            }

            sb.append(node.key);
            sb.append(": ");
            sb.append(node.value);
            sb.append("\n");
            if (node.smaller != null) {
                for(i = 0; i <= indent; ++i) {
                    sb.append("  ");
                }

                sb.append("smaller: ");
                this.show(node.smaller, sb, indent + 1);
            }

            if (node.larger != null) {
                for(i = 0; i <= indent; ++i) {
                    sb.append("  ");
                }

                sb.append("larger: ");
                this.show(node.larger, sb, indent + 1);
            }

        }
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        this.show(this.root, sb, 0);
        return sb.toString();
    }



    class Node {
        final Key key;
        Value value;
        BSTSimple<Key, Value>.Node smaller = null;
        BSTSimple<Key, Value>.Node larger = null;
        int count = 0;

        Node(Key key, Value value) {
            this.key = key;
            this.value = value;
        }

        public String toString() {
            StringBuilder sb = new StringBuilder("Node: " + this.key + ":" + this.value);
            if (this.smaller != null) {
                sb.append(", smaller: " + this.smaller.key);
            }

            if (this.larger != null) {
                sb.append(", larger: " + this.larger.key);
            }

            return sb.toString();
        }
    }

    private class NodeValue {
        private final BSTSimple<Key, Value>.Node node;
        private final Value value;

        NodeValue(BSTSimple<Key, Value>.Node node, Value value) {
            this.node = node;
            this.value = value;
        }

        public String toString() {
            return this.node + "<->" + this.value;
        }
    }

    public static void main(String[] args) {
        int size = 600;
        int keybound = 1200;
        Map<Integer, Integer> map = new HashMap();

        for(int i = 0; i < size; ++i) {
            map.put((int)(Math.random() * (double)keybound), 0);  //generate tree
        }

        BSTdetail<Integer, Integer> bst = new BSTSimple(map);

        ArrayList<Integer> gaps = new ArrayList();

        int Key;

        for(int N = 100; N <= 10000; N += 100) {
            int puts = 0;
            int deletes = 0;
            int maxdepth;

            for(maxdepth = 0; maxdepth < N; ++maxdepth) {
                Random random = new Random();
                Key = (int)(Math.random() * (double)keybound);
                if (random.nextBoolean()) {
                    bst.put(Key, 0);
                    puts+=1;
                } else {
                    bst.delete(Key);
                    deletes+=1;
                }
            }

            maxdepth = bst.getMaxDepth();

            System.out.println("The size of tree start with "+bst.size() + "  operations including ( count:delete: " + deletes + " & put: " + puts + ") Then the depth of the tree after " + N + " operations" + " is " + maxdepth);

            //gaps.add(maxdepth * maxdepth - bst.size());
        }

        /**
        try {
            FileOutputStream fis = new FileOutputStream("./src/result.csv");
            OutputStreamWriter isr = new OutputStreamWriter(fis);
            BufferedWriter bw = new BufferedWriter(isr);
            int n = 100;
            Iterator x = gaps.iterator();

            while(x.hasNext()) {
                Key = (Integer)x.next();
                String content = n + "," + Key + "\n";
                n += 100;
                bw.write(content);
                bw.flush();
            }

            bw.close();
        } catch (IOException x) {
            x.printStackTrace();
        }**/

    }

}
